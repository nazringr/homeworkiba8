package com.company;

public class Cat extends Pet {
    @Override
    public void respond() {
        System.out.println("Hello owner, I am a cat");

    }

    @Override
    public void foul() {
        System.out.println("Cat");

    }
}
