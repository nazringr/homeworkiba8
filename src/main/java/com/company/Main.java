package com.company;

public class Main {

    public static void main(String[] args) {

        Pet fish = new Fish();
        Pet RobotCat = new RoboCat();
        Pet DomesticCat = new DomesticCat();
        Pet cat = new Cat();
        Human w =new Woman();
        Human m= new Man();
        System.out.println("___________Fish_____________");
        fish.foul();
        fish.eat();
        fish.respond();

        System.out.println("___________RobotCat_____________");
        RobotCat.foul();
        RobotCat.eat();
        RobotCat.respond();

        System.out.println("___________Domestic Cat_____________");
        DomesticCat.foul();
        DomesticCat.eat();
        DomesticCat.respond();

        System.out.println("___________Cat_____________");
        cat.foul();
        cat.eat();
        cat.respond();
        System.out.println("___________woman_____________");
        w.greetPet();
        ((Woman) w).makeup();
        System.out.println("___________man_____________");
        m.greetPet();
        ((Man) m).repairCar();






    }
}