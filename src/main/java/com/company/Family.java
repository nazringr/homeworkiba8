package com.company;

import java.util.ArrayList;
import java.util.Objects;

public class Family {

    private Human mother;
    private Human father;
    ArrayList<Human> children = new ArrayList<>();
    private Pet pet;

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pet +
                '}';
    }

    public Family(Human mother, Human father, ArrayList<Human> children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }
    public void addChild(Human human){
        int size= children.size()+1;
        ArrayList<Human> child =new ArrayList<>(size);
        for (int j = 0; j < size-1; j++) child.add(children.get(j));

        child.add(child.size(),human);
        this.children = child;

    }


    public boolean deleteChild(int i) {
        boolean flag = false;
        int l = children.size();
        ArrayList<Human> human = new ArrayList<>(children.size() - 1);

        for (int a = 0; a < l; a++) {
            if (a != l) {
                ArrayList<Human> child =new ArrayList<>(l);
                child.add(i++, children.get(a));

            }
        }
        if (i < l) {
            flag = true;
        } else {
            return false;
        }
        this.children = human;

        return flag;


    }

    public int countFamily() {
        return children.size() + 2;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father);

    }



}